package API_Trigger;

import java.util.List;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
public class Get_RequestSpecification {

	public static void main(String[] args) {
		
		int exp_page = 2;
		int exp_per_page = 6;
		int exp_total = 12;
		int exp_total_pages = 2;
		
		int[] exp_id = {7,8,9,10,11,12};
		String[] exp_email = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		
		String exp_url = "https://reqres.in/#support-heading";
		
		String hostname = "https://reqres.in";
		String resource = "/api/users?page=2";
		
		RequestSpecification req_spec = RestAssured.given();
		
		Response response = req_spec.get(hostname + resource);
		System.out.println(response);
		
		int statuscode = response.getStatusCode();
		System.out.println(statuscode);
		
		ResponseBody responsebody = response.getBody();
		System.out.println(responsebody);
		
		
		
		
        int res_page = responsebody.jsonPath().getInt("page");
        int res_per_page = responsebody.jsonPath().getInt("per_page");
		int res_total = responsebody.jsonPath().getInt("total");
		int res_total_pages = responsebody.jsonPath().getInt("total_pages");
		
		
		List<String>dataArray = responsebody.jsonPath().getList("data");
		int sizeofarray = dataArray.size();
		
		// Step 7.1 : Validate per page parameters
				Assert.assertEquals(res_page, exp_page);
				Assert.assertEquals(res_per_page, exp_per_page);
				Assert.assertEquals(res_total, exp_total);
				Assert.assertEquals(res_total_pages, exp_total_pages);

				// Step 7.2 : Validate data array
				
				for (int i = 0; i < sizeofarray; i++) {
					Assert.assertEquals(Integer.parseInt(responsebody.jsonPath().getString("data["+i+"].id")),exp_id[i],"Validation of id failed for json object at index : " + i);
					Assert.assertEquals(Integer.parseInt(responsebody.jsonPath().getString("data["+i+"].email")),exp_email[i],"Validation of email failed for json object at index : " + i);
					Assert.assertEquals(responsebody.jsonPath().getString("support.url"), exp_url, "Validation of URL failed");
				}
				
				
		
		
		
		

	}

}
