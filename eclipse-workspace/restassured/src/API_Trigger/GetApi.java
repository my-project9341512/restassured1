package API_Trigger;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;
public class GetApi {

	public static void main(String[] args) {
		
		String hostname= "https://reqres.in/";
		String resource = "api/users?page=2";
		
		int[] id = {7,8,9,10,11,12};
		String[] email = {"michael.lawson@reqres.in","lindsay.ferguson@reqres.in","tobias.funke@reqres.in","byron.fields@reqres.in","george.edwards@reqres.in","rachel.howell@reqres.in"};
		String[] first_name= {"Michael","Lindsay","Tobias","Byron","George","Rachel"};
		String[] last_name = {"Lawson","Ferguson","Funke","Fields","Edwards","Howell"};
		String[] avatar = {"https://reqres.in/img/faces/7-image.jpg","https://reqres.in/img/faces/8-image.jpg","https://reqres.in/img/faces/9-image.jpg","https://reqres.in/img/faces/10-image.jpg","https://reqres.in/img/faces/11-image.jpg","https://reqres.in/img/faces/12-image.jpg"};
		
		RestAssured.baseURI = hostname;
			String responsebody=	given().when().get(resource).then().extract().response().asString();
		
		JsonPath jsp_res = new JsonPath(responsebody);
		int count = jsp_res.getInt("data.size()");
		for (int i=0;i<count;i++)
		{
			String exp_id = Integer.toString(id[i]);
			String exp_email = email[i];
			String exp_firstname = first_name[i];
			String exp_lastname = last_name[i];
			String exp_avatar = avatar[i];
			
			String res_id = jsp_res.getString("data.["+i+"].id");
			String res_name = jsp_res.getString("data.["+i+"].id");
			String res_email = jsp_res.getString("data.["+i+"].id");
			String res_firstname= jsp_res.getString("data["+i+"].first_name");
			String res_lastname= jsp_res.getString("data["+i+"].last_name");
			String res_avatar= jsp_res.getString("data["+i+"].avatar");
			
			Assert.assertEquals(res_id, exp_id);
		    Assert.assertEquals(email, exp_email);
			Assert.assertEquals(res_firstname, exp_firstname);
			Assert.assertEquals(res_lastname, exp_lastname);
			Assert.assertEquals(res_avatar, exp_avatar);
			
		}
		
	}
			
		

	}


