package API_Trigger;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class PostApi_requestSpecification {

	public static void main(String[] args) {
		
		String hostname = "https://reqres.in/";
		String resource = "/api/users";
		String headername = "Content-Type";
		String headervalue = "application/json";
		String requestBody = "{\n"
				+ "    \"name\": \"morpheus\",\n"
				+ "    \"job\": \"leader\"\n"
				+ "}";
		
		RequestSpecification req_spec = RestAssured.given();
		
		req_spec.header(headername, headervalue);
		
		req_spec.body(requestBody);
		
		Response response = req_spec.post(hostname+resource);
		
		ResponseBody responsebody = response.getBody();
		System.out.println(responsebody);

		
		int statuscode = response.statusCode();
		System.out.println(statuscode);
		
		String res_name = responsebody.jsonPath().getString("name");
		String res_job = responsebody.jsonPath().getString("job");
		 String res_id =  responsebody.jsonPath().getString("id");
		 String res_createdAt = responsebody.jsonPath().getString("createdAt");
		 res_createdAt = res_createdAt.toString().substring(0,11);
		 
		 JsonPath jsp_req = new JsonPath(requestBody);
		 String req_name = jsp_req.getString("name");
		 String req_job= jsp_req.getString("job");
		 
		 
		 LocalDateTime currentDate = LocalDateTime.now();
		 String expecteddate = currentDate.toString().substring(0,11);
		 
		 Assert.assertEquals(res_name,req_name,"Name in ResponseBody is not equal to Name sent in RequestBody");
		 Assert.assertEquals(res_job,req_job,"Job in ResponseBody is not equal to Job sent in RequestBody");
		 Assert.assertNotNull(res_id,"Id in ResponseBody is found to be null");
		 Assert.assertEquals(res_createdAt,expecteddate,"createdAt in ResponseBody is not equal to Date generated"); 
		 
	}

}
