package API_Trigger;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

public class Post_Api {

	public static void main(String[] args) {

		String hostname = "https://reqres.in/";
		String resource = "/api/users";
		String headername = "Content-Type";
		String headervalue = "application/json";
		String requestBody = "{\n"
				+ "    \"name\": \"morpheus\",\n"
				+ "    \"job\": \"leader\"\n"
				+ "}";
		
		RestAssured.baseURI = hostname;
		
		String Responsebody = given().header(headername,headervalue).body(requestBody).when().post(resource).then().extract().response().asString();
		System.out.println(Responsebody);
		JsonPath jsp_res = new JsonPath(Responsebody);
		
		String res_name = jsp_res.getString("name");
		System.out.println(res_name);
		 String res_job = jsp_res.getString("job");
		 System.out.println(res_job);
		 String res_id = jsp_res.getString("id");
		 System.out.println(res_id);
		 String res_createdAt = jsp_res.getString("createdAt");
		 res_createdAt = res_createdAt.toString().substring(0,11);
		 
		 JsonPath jsp_req = new JsonPath(requestBody);
		 String req_name = jsp_req.getString("name");
		 String req_job = jsp_req.getString("job");
		
		 LocalDateTime currentdate = LocalDateTime.now();
		 String expecteddate = currentdate.toString().substring(0,11);
		 
		 Assert.assertEquals(res_name,req_name);
		 Assert.assertEquals(res_job,req_job);
		 Assert.assertNotNull(res_id);
		 Assert.assertEquals(res_createdAt,expecteddate); 
			
		
		

	}

}
