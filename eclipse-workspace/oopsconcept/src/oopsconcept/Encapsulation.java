package oopsconcept;

public class Encapsulation {

           private String name ;
			private int age;
			
			public String getName() {
				return name;
			}


			public void setName(String name) {
				this.name = name;
			}


			public int getAge() {
				return age;
			}


			public void setAge(int age) {
				this.age = age;
			}


			public void ReadInfo() {
				name= "aruna";
				age = 27;
				System.out.println("My name is " +name+" And Age :" +age);
			}
			
			
			public static void main(String[] args) {
				
			
		Encapsulation obj1 = new Encapsulation();
		obj1.ReadInfo();
		
		}
}

