package environmentandrepositories;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import requestrepository.requestrepository;

public class API_Trigger extends requestrepository{
	
	static String headername = "content-type";
	static String headervalue ="application/jason";
	
	public static Response Post_API_Trigger(String requestbody,String endpoint) {
	RequestSpecification req_spec = RestAssured.given();
	req_spec.header(headername,headervalue);
	req_spec.body(requestbody);
	Response response = req_spec.post(endpoint);
	return response;
	
	
	}
	
	public static Response get_API_Trigger(String responsebody ,String endpoint) {
		RequestSpecification req_spec = RestAssured.given();
		req_spec.header(headername,headervalue);
		req_spec.body(responsebody);
		Response response = req_spec.post(endpoint);
		return response;
		
	}
		
		
	}
	
