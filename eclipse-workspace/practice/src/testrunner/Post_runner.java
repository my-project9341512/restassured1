package testrunner;

import java.io.IOException;

import testscripts.Post_Test_Script;

public class Post_runner {
	
	public static void main(String[] args) throws IOException {
		
		Post_Test_Script.execute();
	}

}
