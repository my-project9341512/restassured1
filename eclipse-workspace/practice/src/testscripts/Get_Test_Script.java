package testscripts;

import java.io.IOException;
import java.util.List;

import org.testng.Assert;

import environmentandrepositories.API_Trigger;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class Get_Test_Script extends API_Trigger {

	ResponseBody responsebody;

	public static void validate(ResponseBody responsebody) {
		int exp_page = 2;
		int exp_per_page = 6;
		int exp_total =  12;
		int exp_total_pages = 2;
		
		int[]  exp_id = {7,8,9,10,11,12};
		String[] exp_email = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		String[] exp_first_name = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String[] last_name = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String[] avatar = { "https://reqres.in/img/faces/7-image.jpg", "https://reqres.in/img/faces/8-image.jpg",
				"https://reqres.in/img/faces/9-image.jpg", "https://reqres.in/img/faces/10-image.jpg",
				"https://reqres.in/img/faces/11-image.jpg", "https://reqres.in/img/faces/12-image.jpg" };

		String exp_Url ="https://reqres.in/#support-heading";
		String exp_text = "To keep ReqRes free, contributions towards server costs are appreciated";
		
		String hostname = "https://reqres.in";
		String resource = "/api/users?page=2";

		
		RequestSpecification req_spec = RestAssured.given();
		
		Response response =req_spec.get(hostname + resource);
		
		int statuscode = response.getStatusCode();
		System.out.println(statuscode);
	
		
		ResponseBody responseBody = response.getBody();
		System.out.println(responsebody);
		
		
		int res_page = responsebody.jsonPath().getInt("page");
		int res_per_page = responsebody.jsonPath().getInt("per_page");
		int res_total = responsebody.jsonPath().getInt("total");
		int res_total_pages = responsebody.jsonPath().getInt("total_pages");
		
		List<String>dataArray = responsebody.jsonPath().getList("data");
		int sizeofArray = dataArray.size();
		
		Assert.assertEquals(res_page, exp_page);
		Assert.assertEquals(res_per_page, exp_per_page);
		Assert.assertEquals(res_total, exp_total);
		Assert.assertEquals(res_total_pages, exp_total_pages);
		
	

	for(int i = 0;i<sizeofArray;i++)
	{
		Assert.assertEquals(Integer.parseInt(responsebody.jsonPath().getString("data[ " + i + "].id")), exp_id[i]);
		Assert.assertEquals(responsebody.jsonPath().getString("data[" + i + "].email"), exp_email[i]);
		Assert.assertEquals(responsebody.jsonPath().getString("data[" + i + "].first_name"), exp_first_name[i]);

		Assert.assertEquals(responsebody.jsonPath().getString("data[" + i + "].last_name"), last_name[i]);

		Assert.assertEquals(responsebody.jsonPath().getString("data[" + i + "].avatar"), avatar[i]);

	}

	// Step 7.3 : Validate support json
	Assert.assertEquals(responsebody.jsonPath().getString("support.url"),exp_Url);Assert.assertEquals(responsebody.jsonPath().getString("support.text"),exp_text);

}
}


