package testscripts;

import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import environmentandrepositories.API_Trigger;
import io.restassured.path.json.JsonPath;
import io.restassured.response.ResponseBody;

public class Post_Test_Script extends API_Trigger {
	
	public static void execute() throws IOException {
		
	}
	
	
	public void validate(ResponseBody responsebody) {
		
		String res_name = responsebody.jsonPath().getString("name");
		String res_job = responsebody.jsonPath().getString("job");
		String res_id = responsebody.jsonPath().getString("id");
		String res_createdAt = responsebody.jsonPath().getString("createdAt");
		 res_createdAt = res_createdAt.toString().substring(0,11);
		 
		 JsonPath jsp_req = new JsonPath(post_request_body());
		 String req_name =  jsp_req.getString("name");
		 String req_job = jsp_req.getString("job");
		 
		 LocalDateTime currentdate = LocalDateTime.now();
		 String expecteddate = currentdate.toString().substring(0, 11);
		 
		 Assert.assertEquals(res_name,req_name,"Responsebody name is not equal to name sent in requestbody");
		 Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
			Assert.assertNotNull(res_id, "Id in ResponseBody is found to be null");
			Assert.assertEquals(res_createdAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");

		 
	
	}

}
