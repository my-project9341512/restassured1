package commonmethod;

import Requestrepository.requestrepository;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class API_Trigger extends requestrepository {
	
	static String headername = "content-type";
	static String headervalue = "application/json";
	
	public static Response Post_API_Trigger(String requestbody,String endpoint) {
		RequestSpecification req_spec = RestAssured.given();
		req_spec.header(headername,headervalue);
		req_spec.body(requestbody);
		Response response  = req_spec.post(endpoint);
		return response;
		
		
				
		
	}

}
