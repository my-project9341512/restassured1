Feature: Trigger Post_API with requested parameters

@Post_API
Scenario: Trigger the API_request with valid request body parameters
   Given Enter Name and Job in requestbody
   When send request with payload
   Then  validate status code
   And validate response body parameters
   
   @Post_API
   Scenario Outline: Test Post_API with multiple data sets
   Given Enter "<Name>" and "<Job>" in requestbody
   When send request with payload
   Then  validate status code
   And validate response body parameters
   
   
   Examples:
   |Name||job|
   |Aruna||Tester|
   