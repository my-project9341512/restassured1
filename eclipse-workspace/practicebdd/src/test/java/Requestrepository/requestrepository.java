package Requestrepository;

import environment.environment;

public class requestrepository extends environment {
	
	public static String Post_request_body() {
		String requestbody = "{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"leader\",\r\n"
				+ "    \"id\": \"8\",\r\n"
				+ "    \"createdAt\": \"2024-05-18T13:43:12.120Z\"\r\n"
				+ "}";
		return requestbody;
	}

}
