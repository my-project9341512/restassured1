package stepDefinitions;

import java.time.LocalDateTime;

import org.testng.Assert;

import Requestrepository.requestrepository;
import commonmethod.API_Trigger;
import environment.environment;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class stepDefinitions {
	String RequestBody;
	String endpoint;
	Response response;
	ResponseBody responsebody;

	@Given("Enter Name and Job in requestbody")
	public void Enter_name_and_Job_in_requestbody() {
	   RequestBody = requestrepository.Post_request_body();
	   endpoint = environment.post_endpoint();
	   
	    
	}
	
	@Given("Enter Name and Job in requestbody")
	public void enter_name_and_job_in_requestbody(String String , String String2) {
	   RequestBody = "{\r\n"
	   		+ "    \"name\": \"String\",\r\n"
	   		+ "    \"job\": \"String2\",\r\n"
	   		 + "}";
	   endpoint = environment.post_endpoint();
	}
	@When("send request with payload")
	public void send_request_with_payload() {
	    Response  response = API_Trigger.Post_API_Trigger(RequestBody, endpoint);
	}
	@Then("validate status code")
	public void validate_status_code() {
	   int statuscode = response.getStatusCode();
	   Assert.assertEquals(statuscode,201,"correct status could not find even after retrying");
	}
	@Then("validate response body parameters")
	public void validate_response_body_parameters() {
	   ResponseBody responsebody = response.getBody();
	   String res_name = responsebody.jsonPath().getString("name");
	   String res_job = responsebody.jsonPath().getString("job");
	   String res_id = responsebody.jsonPath().getString("res_id");
	   String res_createdAt = responsebody.jsonPath().getString("createdAt");
	   res_createdAt= res_createdAt.toString().substring(0, 11);
	   
	   JsonPath jsp_req = new JsonPath(RequestBody);
	   String req_name = jsp_req.getString("name");
	   String req_job = jsp_req.getString("job");
	   
	  
	 LocalDateTime currentdate = LocalDateTime.now();
	 String expecteddate = currentdate.toString().substring(0,11);
	 
	
	 Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
		Assert.assertNotNull(res_id, "Id in ResponseBody is found to be null");
		Assert.assertEquals(res_createdAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");

	   
	}  
	   
	   


}
