package oopsconceptpractice;

public class encapsulation {
	
	private int age;
	private String name;
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public void readinfo() {
		name = "aruna";
		age = 27;
		System.out.println("my name is aruna");
	}
	
	public static void main (String[] args) {
		encapsulation obj = new encapsulation();
		obj.readinfo();
		obj.getAge();
	}
}
