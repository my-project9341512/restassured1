package testScripts;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import commonmethods.API_Trigger;
import commonmethods.Testng_Retry_Analyzer;
import commonmethods.Utilities;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Delete_Test_Script extends API_Trigger {
	File logfolder;
	Response response;
	ResponseBody responseBody;

	@BeforeTest
	public void setup() {
		logfolder = Utilities.createFolder("Delete_Api");
	}

	@Test (retryAnalyzer = Testng_Retry_Analyzer.class , description = "Validate the responseBody parameters of Delete_TC_1")
	public void validate() {
		response = Delete_API_Trigger(delete_request_body(), delete_endpoint());
		responseBody = response.getBody();
		int statuscode = response.statusCode();

		Assert.assertEquals(statuscode, 204, "Correct status code not found even after retrying for 5 times");

	}

	@AfterTest
	public void teardown() throws IOException {
		Utilities.createLogFile("Delete_Api_TC02", logfolder, delete_endpoint(), delete_request_body(),
				response.getHeaders().toString(), responseBody.asString());
	}
}
