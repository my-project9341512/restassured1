package Api_Trigger_reference;

import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;

public class Delete_reference {

	public static void main(String[] args) {
		String hostname = "https://reqres.in/";
		String resource = "api/users/2";
		
		
		RestAssured.baseURI = hostname;
		
		String Responsebody = given().delete(resource).then().log().all().extract().response().asString();

		System.out.println(Responsebody);

	}

}
