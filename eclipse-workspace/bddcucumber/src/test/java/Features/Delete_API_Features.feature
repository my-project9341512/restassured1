@Delete_API
Feature: delete API with the required parameters

@Delete_API
Scenario: Trigger API for deleting user
Given  Enter endpoint for deleteing user
When Send delete request with payload
Then Validate delete response status code 
And The responsebody should be empty