Feature: Trigger the Patch API with required params

@Patch_Api
Scenario: Trigger Patch API request with valid request body params
    Given Update the NAME and JOB in sent request body
    When Send Patch with request payload 
    Then Validate status code 200
    And Validate response body params as given
    
@Patch_Api   
Scenario Outline: Trigger Patch API request with valid request body params
    Given Update the "<NAME>" and "<JOB>" in sent request body
    When Send Patch with request payload 
    Then Validate status code 200
    And Validate response body params as given
    
Examples:
    |NaMe|JOB|
    |ArUnA|TESTER|
    |PrIsHa|DEV|
    |SuBhASh|BA|