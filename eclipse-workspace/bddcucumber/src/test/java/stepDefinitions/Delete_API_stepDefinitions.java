package stepDefinitions;

import org.testng.Assert;

import Environmentandrepository.Environment;
import commonmethods.API_Trigger;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Delete_API_stepDefinitions {
	
	String endpoint;
	Response response;
	ResponseBody responseBody;

	@Given("Enter endpoint for deleteing user")
	public void enter_endpoint_for_deleteing_user() {
      endpoint = Environment.delete_endpoint();
//	    throw new io.cucumber.java.PendingException();
	}
	

		@When("Send delete request with payload")
		public void send__delete_request_with_payload() {
			response = API_Trigger.Delete_API_Trigger(endpoint, endpoint);
		   }

	  
	
	@Then("Validate delete response status code")
	public void validate_delete_response_status_code() {
		int statuscode = response.statusCode();
		Assert.assertEquals(statuscode, 204, "correct statuscode  and even after retrying for 5 time");

//	    throw new io.cucumber.java.PendingException();
	}
	@Then("The responsebody should be empty")
	public void the_responsebody_should_be_empty() {
		
//	    throw new io.cucumber.java.PendingException();
	}


}
