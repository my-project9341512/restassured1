package stepDefinitions;

import java.time.LocalDateTime;

import org.testng.Assert;

import Environmentandrepository.Environment;
import Environmentandrepository.RequestRepository;
import commonmethods.API_Trigger;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Patch_API_stepDefinitions {
	
	String requestbody;
	String endpoint;
	Response response;
	ResponseBody responseBody;
	
	@Given("Update the NAME and JOB in sent request body")
	public void update_the_name_and_job_in_sent_request_body() {
		requestbody = RequestRepository.patch_request_body();
		endpoint = Environment.Patch_endpoint();
	}
	
	@Given("Update the {string} and {string} in sent request body")
	public void update_the_and_in_sent_request_body(String string, String string2) {
		endpoint = Environment.Patch_endpoint();
		requestbody = "{\r\n"
				+ "    \"name\": \""+string+"\",\r\n"
				+ "    \"job\": \""+string2+"\"\r\n"
				+ "}";

	}


	@When("Send Patch with request payload")
	public void send_patch_with_request_payload() {
		response = API_Trigger.Put_API_Trigger(requestbody, endpoint);
	  
	}
	@Then("Validate status code {int}")
	public void validate_status_code(Integer int1) {
		int statuscode = response.statusCode();
		Assert.assertEquals(statuscode, 200, "Correct status code not found even after retrying for 5 times");
	   
	}
	@Then("Validate response body params as given")
	public void validate_response_body_params_as_given() {
		responseBody = response.getBody();
		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.toString().substring(0, 11);

		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
		Assert.assertEquals(res_updatedAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");

	    //throw new io.cucumber.java.PendingException();
	}



		
	}


